---
layout: handbook-page-toc
title: CSM Segments
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [CSM Handbook homepage](/handbook/customer-success/tam/) for additional CSM-related handbook pages.

---

## Overview

The Customer Success Manager organization is distributed across different customer segments, based on customer size and Annual Recurring Revenue.

## Segment criteria

Segment criteria are [defined in the Customer Success Manager wiki](https://gitlab.com/gitlab-com/customer-success/tam/-/wikis/tam-segments).

## Segments

[<button class="btn btn-primary" type="button">Digital Touch</button>](digital/)
[<button class="btn btn-primary" type="button">Scale</button>](scale/)
[<button class="btn btn-primary" type="button">Growth</button>](mid-touch/)
[<button class="btn btn-primary" type="button">Strategic</button>](strategic/)
